#include "mqtt.hpp"
#include <esp_event.h>
#include <esp_log.h>

namespace {
const char* TAG = "mqtt";
}

const std::string Mqtt::HA_CLIMATE_CFG_DATA{"\
{\
  \"name\": \"Climate Livingroom\",\
  \"mode_cmd_t\":\"homeassistant/climate/livingroom/thermostatModeCmd\",\
  \"mode_stat_t\":\"homeassistant/climate/livingroom/state\",\
  \"mode_stat_tpl\":\"{{ value_json.mode }}\",\
  \"avty_t\":\"homeassistant/climate/livingroom/available\",\
  \"pl_avail\":\"online\",\
  \"pl_not_avail\":\"offline\",\
  \"temp_cmd_t\":\"homeassistant/climate/livingroom/targetTempCmd\",\
  \"temp_stat_t\":\"homeassistant/climate/livingroom/state\",\
  \"temp_stat_tpl\":\"{{ value_json.target_temp }}\",\
  \"curr_temp_t\":\"homeassistant/climate/livingroom/state\",\
  \"curr_temp_tpl\":\"{{ value_json.current_temp }}\",\
  \"min_temp\":\"15\",\
  \"max_temp\":\"35\",\
  \"temp_step\":\"0.5\",\
  \"modes\":[\"off\", \"heat\"]\
}"};

void Mqtt::eventHandlerWrapper(void* handlerArgs, esp_event_base_t base, int32_t eventId, void* eventData)
{
    Mqtt* mqtt = reinterpret_cast<Mqtt*>(handlerArgs);
    mqtt->eventHandler(handlerArgs, base, eventId, eventData);
}

void Mqtt::eventHandler(void* handlerArgs, esp_event_base_t base, int32_t eventId, void* eventData)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, eventId);
    esp_mqtt_event_handle_t event = reinterpret_cast<esp_mqtt_event_handle_t>(eventData);
    esp_mqtt_client_handle_t client = event->client;
    switch (static_cast<esp_mqtt_event_id_t>(eventId)) {
        case MQTT_EVENT_CONNECTED:
        {
            ESP_LOGI(TAG, "Mqtt client connected");
            esp_mqtt_client_publish(client, "homeassistant/climate/livingroom/state", "{\"mode\":\"off\",\"target_temp\":\"20.0\",\"current_temp\":\"21.4\"}", 0, 1, 0);
            esp_mqtt_client_publish(client, "homeassistant/climate/livingroom/available", "online", 0, 1, 1);
            esp_mqtt_client_publish(client, "homeassistant/climate/livingroom/config", HA_CLIMATE_CFG_DATA.c_str(), 0, 1, 0);
            esp_mqtt_client_subscribe(client, "homeassistant/climate/livingroom/thermostatModeCmd", 1);
            esp_mqtt_client_subscribe(client, "homeassistant/climate/livingroom/targetTempCmd", 1);
            break;
        }
        case MQTT_EVENT_DISCONNECTED:
        {
            break;
        }
        case MQTT_EVENT_ERROR:
        {
            ESP_LOGD(TAG, "MQTT_EVENT_ERROR");
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
                ESP_LOGE(TAG, "Error: reported from esp-tls. Code: 0x%x", event->error_handle->esp_tls_last_esp_err);
                ESP_LOGE(TAG, "Error: reported from tls stack. Code: 0x%x", event->error_handle->esp_tls_stack_err);
                ESP_LOGE(TAG, "Error: captured as transport's socket errno. Code: 0x%x",  event->error_handle->esp_transport_sock_errno);
                ESP_LOGE(TAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));
            }
            break;
        }
        case MQTT_EVENT_SUBSCRIBED:
        {
            ESP_LOGD(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        }
        case MQTT_EVENT_PUBLISHED:
        {
            ESP_LOGD(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        }
        case MQTT_EVENT_DATA:
        {
            ESP_LOGD(TAG, "MQTT_EVENT_DATA");
            //printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            //printf("DATA=%.*s\r\n", event->data_len, event->data);
            bool dataChanged{false};
            if (strncmp("homeassistant/climate/livingroom/thermostatModeCmd", event->topic, event->topic_len) == 0) {
                if (strncmp("off", event->data, event->data_len) == 0) {
                    this->heat = false;
                    ESP_LOGD(TAG, "New mode: off");
                } else {
                    this->heat = true;
                    ESP_LOGD(TAG, "New mode: heat");
                }
                if (heatStateChanged)
                    heatStateChanged(this->heat);
                dataChanged = true;
            }
            if (strncmp("homeassistant/climate/livingroom/targetTempCmd", event->topic, event->topic_len) == 0) {
                this->targetTemperature = atof(event->data);
                ESP_LOGD(TAG, "new target temp");
                if (targetTempChanged)
                    targetTempChanged(this->targetTemperature);
                dataChanged = true;
            }
            if (dataChanged)
                publishState();
            break;
        }
        default:
        {
            break;
        }
    }
}

void Mqtt::publishState()
{
    if (!client) {
        ESP_LOGE(TAG, "Temperature publishing failed. Client not initiated.");
        return;
    }
    std::string msg{};
    char buff[20];
    msg += "{\"mode\":";
    msg += (heat) ? "\"heat\"" : "\"off\"";
    msg += ",\"target_temp\":";
    sprintf(buff, "\"%0.1f\"", targetTemperature);
    msg += buff;
    msg += ",\"current_temp\":";
    sprintf(buff, "\"%0.1f\"", temperature);
    msg += buff;
    msg += "}";
    esp_mqtt_client_publish(client, "homeassistant/climate/livingroom/state", msg.c_str(), 0, 1, 0);
}

void Mqtt::init()
{
    esp_mqtt_client_config_t cfg;
    memset(&cfg, 0, sizeof(esp_mqtt_client_config_t));
    cfg.uri = MQTT_SERVER_URI;
    cfg.port = MQTT_SERVER_PORT;
    cfg.username = MQTT_USER;
    cfg.password = MQTT_PASSWORD;
    cfg.lwt_qos = 1;
    cfg.lwt_msg = "offline";
    cfg.lwt_msg_len = 7;
    cfg.lwt_retain = 1;
    cfg.lwt_topic = "homeassistant/climate/livingroom/available";

    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    client = esp_mqtt_client_init(&cfg);
    if (client == NULL)
    {
        ESP_LOGE(TAG, "Initialization failed");
        abort();
    }
    ESP_ERROR_CHECK(esp_mqtt_client_register_event(client, MQTT_EVENT_ANY, eventHandlerWrapper, this));
    ESP_ERROR_CHECK(esp_mqtt_client_start(client));
}

void Mqtt::publishTemperature(double temperature)
{
    this->temperature = temperature;
    publishState();
}

void Mqtt::publishTargetTemperature(double targetTemperature)
{
    this->targetTemperature = targetTemperature;
    publishState();
}

void Mqtt::publishHeatState(bool heatState)
{
    this->heat = heatState;
    publishState();
}

void Mqtt::subscribeTargetTemperature(TempChanged targetTempChanged)
{
    this->targetTempChanged = targetTempChanged;
}

void Mqtt::subscribeHeatState(StateChanged heatStateChanged)
{
    this->heatStateChanged = heatStateChanged;
}