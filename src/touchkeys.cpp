#include "touchkeys.hpp"
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include <esp_log.h>

#define TAG "TouchKeys"

TouchKeys::TouchKeys()
    : gpioEventQueue{NULL}
    , isrDataLeft{GPIO_TOUCHKEY_LEFT, this}
    , isrDataRight{GPIO_TOUCHKEY_RIGHT, this}
{
}

void TouchKeys::gpioTaskHandlerWrapper(void* args)
{
    TouchKeys* obj = reinterpret_cast<TouchKeys*>(args);
    gpio_num_t gpio;
    for(;;) {
        if(xQueueReceive(obj->gpioEventQueue, &gpio, portMAX_DELAY)) {
            bool pushed = (gpio_get_level(gpio) == 0) ? false : true;
            obj->gpioTaskHandler(gpio, pushed);
        }
    }
}

void TouchKeys::gpioTaskHandler(gpio_num_t gpio, bool pushed)
{
    switch(gpio) {
        case GPIO_TOUCHKEY_LEFT:
        {
            if (eventCallbackLeft)
                eventCallbackLeft(pushed);
            break;
        }
        case GPIO_TOUCHKEY_RIGHT:
        {
            if (eventCallbackRight)
                eventCallbackRight(pushed);
            break;
        }
        default:
        {
            ESP_LOGE(TAG, "Unknown GPIO[%d]", gpio);
            break;
        }
    }
}

void IRAM_ATTR TouchKeys::gpioIsrHandlerWrapper(void* args)
{
    IsrData* isrData = reinterpret_cast<IsrData*>(args);
    xQueueSendFromISR(isrData->touckKeys->gpioEventQueue, &(isrData->gpio), NULL);
}

void TouchKeys::init()
{
    gpio_config_t ioConfig = {};
    ioConfig.intr_type = GPIO_INTR_ANYEDGE;
    ioConfig.mode = GPIO_MODE_INPUT;
    ioConfig.pin_bit_mask = ((1ULL << GPIO_TOUCHKEY_LEFT) | (1ULL << GPIO_TOUCHKEY_RIGHT));
    ioConfig.pull_down_en = GPIO_PULLDOWN_DISABLE;
    ioConfig.pull_up_en = GPIO_PULLUP_DISABLE;
    ESP_ERROR_CHECK(gpio_config(&ioConfig));

    gpioEventQueue = xQueueCreate(10, sizeof(TouchKeys*));
    xTaskCreate(gpioTaskHandlerWrapper, "touchGpioHandler", 2048, this, 10, NULL);

    ESP_ERROR_CHECK(gpio_install_isr_service(ESP_INTR_FLAG_EDGE));
    ESP_ERROR_CHECK(gpio_isr_handler_add(GPIO_TOUCHKEY_LEFT, gpioIsrHandlerWrapper, &isrDataLeft));
    ESP_ERROR_CHECK(gpio_isr_handler_add(GPIO_TOUCHKEY_RIGHT, gpioIsrHandlerWrapper, &isrDataRight));
}

void TouchKeys::registerLeftEvent(KeyEvent callback)
{
    this->eventCallbackLeft = callback;
}

void TouchKeys::registerRightEvent(KeyEvent callback)
{
    this->eventCallbackRight = callback;
}

bool TouchKeys::getLeftKeyValue()
{
    return gpio_get_level(GPIO_TOUCHKEY_LEFT) != 0;  
}

bool TouchKeys::getRightKeyValue()
{
    return gpio_get_level(GPIO_TOUCHKEY_RIGHT) != 0;
}