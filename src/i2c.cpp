#include "i2c.hpp"
#include <driver/i2c.h>
#include <driver/gpio.h>
#include <esp_log.h>

#define TAG "i2c master"

void I2cMaster::init()
{
    i2c_config_t i2c_conf{};
    i2c_conf.mode = I2C_MODE_MASTER;
    i2c_conf.sda_io_num = I2C_MASTER_SDA_IO;
    i2c_conf.scl_io_num = I2C_MASTER_SCL_IO;
    i2c_conf.sda_pullup_en = true;
    i2c_conf.scl_pullup_en = true;
    i2c_conf.master.clk_speed = I2C_MASTER_FREQ_HZ;

    ESP_ERROR_CHECK(i2c_param_config(I2C_NUM_0, &i2c_conf));
    ESP_ERROR_CHECK(i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0));
}

esp_err_t I2cMaster::write(I2cAddress address, const I2cData& byteData, const I2cData& seqData)
{
    auto cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, true));

    for(auto writeByte : byteData)
    {
        ESP_ERROR_CHECK(i2c_master_write_byte(cmd, writeByte, true));
    }

    if (seqData.size())
    {
        ESP_ERROR_CHECK(i2c_master_write(cmd, seqData.data(), seqData.size(), true));
    }

    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    auto espRc = i2c_master_cmd_begin(I2C_NUM_0, cmd, 10/portTICK_PERIOD_MS);
	if (espRc != ESP_OK) {
        ESP_LOGE(TAG, "Write data failed. Error code: 0x%02X", espRc);
	}

    i2c_cmd_link_delete(cmd);
    return espRc;
}

esp_err_t I2cMaster::readRegister(I2cAddress address, I2cAddress regAddress, I2cData& data)
{
    auto size = data.size();
    auto cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, true));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, regAddress, true));
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_READ, true));
    if (size > 1)
    {
        ESP_ERROR_CHECK(i2c_master_read(cmd, data.data(), size - 1, I2C_MASTER_ACK));
    }
    ESP_ERROR_CHECK(i2c_master_read_byte(cmd, data.data() + size - 1, I2C_MASTER_NACK));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    auto espRc = i2c_master_cmd_begin(I2C_NUM_0, cmd, 10/portTICK_PERIOD_MS);
	if (espRc != ESP_OK) {
        ESP_LOGE(TAG, "Read data failed. Error code: 0x%08X", espRc);
	}
    i2c_cmd_link_delete(cmd);
    return espRc;
}