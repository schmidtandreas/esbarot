#include "wifi.hpp"
#include <cstring>
#include <esp_wifi.h>
#include <esp_log.h>

static const char* TAG = "Wifi";

void Wifi::eventHandlerWrapper(void* eventHandlerArg, esp_event_base_t eventBase, int32_t eventId, void *eventData)
{
    Wifi* wifi = reinterpret_cast<Wifi*>(eventHandlerArg);
    wifi->eventHandler(eventHandlerArg, eventBase, eventId, eventData);
}

void Wifi::eventHandler(void* eventHandlerArg, esp_event_base_t eventBase, int32_t eventId, void *eventData)
{
    if (eventBase == WIFI_EVENT && eventId == WIFI_EVENT_STA_START)
    {
        ESP_ERROR_CHECK(esp_wifi_connect());
    }
    else if (eventBase == WIFI_EVENT && eventId == WIFI_EVENT_STA_DISCONNECTED)
    {
        xEventGroupClearBits(eventGroupHandle, WIFI_CONNECTED_BIT);
        wifi_event_sta_disconnected_t* data = (wifi_event_sta_disconnected_t*)eventData;
        ESP_LOGI(TAG, "disconnected reason: %u", data->reason);
        ESP_LOGI(TAG, "SSID: %s len: %u", data->ssid, data->ssid_len);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        ESP_ERROR_CHECK(esp_wifi_connect());
        ESP_LOGI(TAG, "Retry to connect to the AP: %s pass: %s", WIFI_SSID, WIFI_PASSWORD);
    } 
    else if (eventBase == IP_EVENT && eventId == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t* event = reinterpret_cast<ip_event_got_ip_t*>(eventData);
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        xEventGroupSetBits(eventGroupHandle, WIFI_CONNECTED_BIT);
    }
}

void Wifi::init()
{
    eventGroupHandle = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_netif_init());
    esp_netif_create_default_wifi_sta();
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &eventHandlerWrapper,
                                                        this,
                                                        &eventInstanceAnyId));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &eventHandlerWrapper,
                                                        this,
                                                        &eventInstanceGotIp));

    wifi_config_t wifi_config;
    std::memcpy(wifi_config.sta.ssid, WIFI_SSID, strlen(WIFI_SSID) + 1);
    std::memcpy(wifi_config.sta.password, WIFI_PASSWORD, strlen(WIFI_PASSWORD) + 1);
    wifi_config.sta.bssid_set = false;
	wifi_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
    wifi_config.sta.pmf_cfg = {
                .capable = true,
                .required = false
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
}

bool Wifi::connect()
{
    EventBits_t bits = xEventGroupWaitBits(eventGroupHandle,
            WIFI_CONNECTED_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);
    
    if (bits & WIFI_CONNECTED_BIT)
    {
        return true;
    }
    return false;
}