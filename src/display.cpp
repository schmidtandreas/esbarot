#include "display.hpp"
#include <esp_err.h>

Display::Display(Timer& timer, I2cMaster& i2c)
    : timer{timer}
    , i2c{i2c}
    , blinkerToggle{false}
    , blinkWifi{false}
    , blinkHeat{false}
{
}

void Display::init()
{
    ESP_ERROR_CHECK(i2c.write(OLED_I2C_ADDRESS, {
        OLED_CONTROL_BYTE_CMD_STREAM,
        OLED_CMD_SET_CHARGE_PUMP,
        0x14,
        OLED_CMD_SET_SEGMENT_REMAP,
        OLED_CMD_SET_COM_SCAN_MODE,
        OLED_CMD_DISPLAY_ON
    }, {}));
    timer.registerTimer(timerName, [this](){blinker();});
}

void Display::clear()
{
    for (uint8_t page = 0; page < 8; page++)
        drawPage(page, 0, I2cData(128, 0));
}

void Display::setContrast(const uint8_t value)
{
    ESP_ERROR_CHECK(i2c.write(OLED_I2C_ADDRESS, {
                              OLED_CMD_SET_CONTRAST,
                              value
    }, {}));
}

void Display::startBlinkWifi()
{
    blinkWifi = true;
    timer.startTimer(timerName, 300000);
}

void Display::stopBlinkWifi()
{
    timer.stopTimer(timerName);
    blinkWifi = false;
}

void Display::printWifi()
{
    drawWifi(true);
}

void Display::clearWifi()
{
    drawWifi(false);
}

void Display::printHeat()
{
    drawHeat(true);
}

void Display::clearHeat()
{
    drawHeat(false);
}

void Display::printHysteresis()
{
    drawHysteresis(true);
}

void Display::clearHysteresis()
{
    drawHysteresis(false);
}

void Display::printThermostat()
{
    I2cData page0Data{thermoIcon[0]};
    I2cData page1Data{thermoIcon[1]};
    I2cData page2Data{thermoIcon[2]};
    I2cData page3Data{thermoIcon[3]};
    I2cData page4Data{thermoIcon[4]};
    I2cData page5Data{thermoIcon[5]};
    drawPage(2, 0, page0Data);
    drawPage(3, 0, page1Data);
    drawPage(4, 0, page2Data);
    drawPage(5, 0, page3Data);
    drawPage(6, 0, page4Data);
    drawPage(7, 0, page5Data);
}

void Display::printThemperature(const double value)
{
    //TODO check this casting (0.8 -> 0.7)
    uint16_t val = static_cast<uint16_t>(value * 10);
    uint8_t num0 = (val) % 10;
    uint8_t num1 = (val / 10) % 10;
    uint8_t num2 = (val / 100) % 10;
    uint8_t offset = 26;

    if (num2 == 0)
        num2 = 10;

    drawPage(3, offset, bigNumberIcons[num2][0]);
    drawPage(4, offset, bigNumberIcons[num2][1]);
    drawPage(5, offset, bigNumberIcons[num2][2]);
    drawPage(6, offset, bigNumberIcons[num2][3]);

    drawPage(3, offset + 24, bigNumberIcons[num1][0]);
    drawPage(4, offset + 24, bigNumberIcons[num1][1]);
    drawPage(5, offset + 24, bigNumberIcons[num1][2]);
    drawPage(6, offset + 24, bigNumberIcons[num1][3]);

    drawPage(6, offset + 49, commaIcon[0]);
    drawPage(7, offset + 49, commaIcon[1]);

    drawPage(4, offset + 56, smallNumberIcons[num0][0]);
    drawPage(5, offset + 56, smallNumberIcons[num0][1]);
    drawPage(6, offset + 56, smallNumberIcons[num0][2]);

    drawPage(3, offset + 56 + 20, degCIcon[0]);
    drawPage(4, offset + 56 + 20, degCIcon[1]);
    drawPage(5, offset + 56 + 20, degCIcon[2]);
    drawPage(6, offset + 56 + 20, degCIcon[3]);

}

void Display::printThresholdTemp(const double value)
{
    uint16_t val = static_cast<uint16_t>(value * 10);
    uint8_t num0 = (val) % 10;
    uint8_t num1 = (val / 10) % 10;
    uint8_t num2 = (val / 100) % 10;
    uint8_t offset = 80;

    if (num2 == 0)
        num2 = 10;

    drawPage(0, offset, tinyNumberIcons[num2][0]);
    drawPage(1, offset, tinyNumberIcons[num2][1]);

    drawPage(0, offset + 10, tinyNumberIcons[num1][0]);
    drawPage(1, offset + 10, tinyNumberIcons[num1][1]);

    drawPage(1, offset + 19, tinyCommaIcon[0]);

    drawPage(0, offset + 22, tinyNumberIcons[num0][0]);
    drawPage(1, offset + 22, tinyNumberIcons[num0][1]);
   
    drawPage(0, offset + 32, tinyDigCIcon[0]);
    drawPage(1, offset + 32, tinyDigCIcon[1]);
}

void Display::drawWifi(const bool enable)
{
    std::vector<uint8_t> page0Data{wifiIcon[0]};
    std::vector<uint8_t> page1Data{wifiIcon[1]};
    if (!enable)
    {
        std::vector<uint8_t> zero(16, 0);
        page0Data = zero;
        page1Data = zero;
    }
    drawPage(0, 0, page0Data);
    drawPage(1, 0, page1Data);
}

void Display::drawHeat(const bool enable)
{
    std::vector<uint8_t> page0Data{heatIcon[0]};
    std::vector<uint8_t> page1Data{heatIcon[1]};
    if (!enable)
    {
        std::vector<uint8_t> zero(16, 0);
        page0Data = zero;
        page1Data = zero;
    }
    drawPage(0, 18, page0Data);
    drawPage(1, 18, page1Data);
}

void Display::drawHysteresis(const bool enable)
{
    std::vector<uint8_t> page0Data{hystIcon[0]};
    std::vector<uint8_t> page1Data{hystIcon[1]};
    if (!enable)
    {
        std::vector<uint8_t> zero(16, 0);
        page0Data = zero;
        page1Data = zero;
    }
    drawPage(0, 35, page0Data);
    drawPage(1, 35, page1Data);
}

void Display::drawPage(const uint8_t page, const uint8_t offset, const I2cData& data)
{
    uint8_t lowOffset = (offset & 0x0F);
    uint8_t highOffset = (offset >> 4) | 0x10;
    uint8_t pageAddr = OLED_CMD_SET_PAGE_START_ADDR + (page & 0x7); // mod 8
    ESP_ERROR_CHECK(i2c.write(OLED_I2C_ADDRESS, {
                              OLED_CONTROL_BYTE_CMD_STREAM,
                              lowOffset,
                              highOffset,
                              pageAddr
    }, {}));
    ESP_ERROR_CHECK(i2c.write(OLED_I2C_ADDRESS, {OLED_CONTROL_BYTE_CMD_DATA}, data));
}

void Display::blinker()
{
    if (blinkWifi)
        drawWifi(blinkerToggle);
    if (blinkHeat)
        drawHeat(blinkerToggle);

    blinkerToggle = ! blinkerToggle;      
}