#include "timer.hpp"
#include <esp_timer.h>

void Timer::init()
{
    auto ret = esp_timer_init();
    if (ret != ESP_ERR_INVALID_STATE) // Invalid state will be returned if timer already  initialized
        ESP_ERROR_CHECK(ret);
}

void Timer::timerHandlerWrapper(void* args)
{
    TimerCbData* data = reinterpret_cast<TimerCbData*>(args);
    data->callback();
}

void Timer::registerTimer(const std::string& name, TimerCallback callback)
{
    auto timer = timers.find(name);
    if (timer != timers.end())
        return;

    esp_timer_handle_t timerHandle;
    TimerCbData* cbData = new TimerCbData();
    cbData->callback = callback;
    esp_timer_create_args_t cfg{};
    cfg.callback = timerHandlerWrapper;
    cfg.name = name.c_str();
    cfg.arg = cbData;
    ESP_ERROR_CHECK(esp_timer_create(&cfg, &timerHandle));
    timers.emplace(name, TimerData{cbData, timerHandle});
}

void Timer::startTimer(const std::string& name, uint64_t period)
{
    auto timer = timers.find(name);
    if (timer == timers.end())
        return;
    
    auto timerHandle = timer->second.handle;
    ESP_ERROR_CHECK(esp_timer_start_periodic(timerHandle, period));
}

void Timer::stopTimer(const std::string& name)
{
    auto timer = timers.find(name);
    if (timer == timers.end())
        return;
    
    auto timerHandle = timer->second.handle;
    ESP_ERROR_CHECK(esp_timer_stop(timerHandle));
}

void Timer::deleteTimer(const std::string& name)
{
    auto timer = timers.find(name);
    if (timer == timers.end())
        return;

    auto timerHandle = timer->second.handle;
    auto cbData = timer->second.cBdata;
    esp_timer_delete(timerHandle);
    delete cbData;
    timers.erase(timer);
}