#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include <esp_log.h>
#include "nvm.hpp"
#include "timer.hpp"
#include "bmp280.hpp"
#include "i2c.hpp"
#include "display.hpp"
#include "wifi.hpp"
#include "mqtt.hpp"
#include "touchkeys.hpp"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_system.h>
#include <esp_event.h>

Nvm nvm{};
Timer timer{};
I2cMaster i2c{};
Bmp280 bmp280{i2c};
Display display{timer, i2c};
Wifi wifi{};
Mqtt mqtt{};
TouchKeys touchKeys{};

double tempOld = -1.0;

extern "C" {
void app_main() {
    nvm.init();
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    timer.init();
    i2c.init();
    bmp280.init();
    display.init();
    display.setContrast(60);
    display.clear();
    display.startBlinkWifi();
    wifi.init();
    touchKeys.init();

    display.printThermostat();

    auto touchEventLeft = [&incThresholdTemp](bool pushed) {
        if (! pushed)
        {
        }
        else
        {
        }
    };

    auto touchEventRight = [&decThresholdTemp](bool pushed) {
        if (! pushed)
        {
        }
        else
        {
        }
    };

    display.printThresholdTemp(20.0);
    touchKeys.registerLeftEvent(touchEventLeft);
    touchKeys.registerRightEvent(touchEventRight);

    auto updateTemp = []() {
        double temp;
        double press;
        if (bmp280.getData(temp, press)) {
            if (static_cast<uint16_t>(temp * 10) != static_cast<uint16_t>(tempOld * 10))
            {
                display.printThemperature(temp);
                mqtt.publishTemperature(temp);
                tempOld = temp;
            }
        }
    };

    timer.registerTimer("read_temp", updateTemp);
    updateTemp();
    timer.startTimer("read_temp", 5000000);

    mqtt.subscribeHeatState([](bool heatState){
        if (heatState)
            display.printHeat();
        else
            display.clearHeat();
    });

    mqtt.subscribeTargetTemperature([](double targetTemp) {
        display.printThresholdTemp(targetTemp);
    });

    //TODO create reconnect idle loop
    auto wifiConnected = wifi.connect();
    display.stopBlinkWifi();
    if (wifiConnected)
    {
        display.printWifi();
        mqtt.init();
    }
    else
    {
        display.clearWifi();
        ESP_LOGE("main", "Wifi coulnd't be initialized.");
    }
}
}