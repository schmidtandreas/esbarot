#include "bmp280.hpp"
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#define TAG "BMP280"

Bmp280::Bmp280(I2cMaster& i2c)
    :i2c{i2c}
{
}

void Bmp280::init()
{
    ESP_ERROR_CHECK(checkChipId());

    // For compensation we need the comp data
    ESP_ERROR_CHECK(getCompensationData());

    // We use recommendatation from Bosch for the use case "Indoor navigation"
    // | use case   | mode   | Oversampl  | osrs_p | osrs_t | IIR | Timing   | ODR  | BW   |
    // |------------|--------|------------|--------|--------|-----|----------|------|------|
    // | Indoor     | Normal | Ultra high |    ×16 |     ×2 |  16 | tstandby | 26.3 | 0.55 |
    // | navigation |        | resolution |        |        |     | =0.5 ms  |      |      |

    ESP_ERROR_CHECK(setRegister(BMP280_REG_CONFIG,
                     BMP280_CONF_IIR_16 |
                     BMP280_CONF_T_SB_05));
    ESP_ERROR_CHECK(setRegister(BMP280_REG_CTRL_MEAS, 
                     BMP280_CTRL_NORMAL |
                     BMP280_CTRL_OSRS_P_16 |
                     BMP280_CTRL_OSRS_T_2));
}

esp_err_t Bmp280::checkChipId()
{
    I2cData data;
    data.resize(1);

    int count = 5;
    while(count--)
    {
        if (i2c.readRegister(BMP280_I2C_ADDRESS, BMP280_REG_ID, data) == ESP_OK)
        {
            ESP_LOGI(TAG, "Chip Id: 0x%02X", data[0]);
            if (data[0] == BMP280_ID)
                return ESP_OK;
            else
                return ESP_FAIL;
        }

        vTaskDelay(1/portTICK_PERIOD_MS);
    }

    return ESP_FAIL;
}

esp_err_t Bmp280::setRegister(I2cAddress reg, uint8_t value)
{
    return i2c.write(BMP280_I2C_ADDRESS, {reg, value}, {});
}

esp_err_t Bmp280::getCompensationData()
{
    if (!getCompensationValue(BMP280_REG_CALIB_T1, compData.dig_T1))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_T2, compData.dig_T2))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_T3, compData.dig_T3))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P1, compData.dig_P1))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P2, compData.dig_P2))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P3, compData.dig_P3))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P4, compData.dig_P4))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P5, compData.dig_P5))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P6, compData.dig_P6))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P7, compData.dig_P7))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P8, compData.dig_P8))
        return ESP_FAIL;
    if (!getCompensationValue(BMP280_REG_CALIB_P9, compData.dig_P9))
        return ESP_FAIL;
    return ESP_OK;
}

bool Bmp280::getCompensationValue(const I2cAddress reg, uint16_t& dig)
{
    int16_t sdig;
    auto ret = getCompensationValue(reg, sdig);
    dig = static_cast<uint16_t>(sdig);
    return ret;
}

bool Bmp280::getCompensationValue(const I2cAddress reg, int16_t& dig)
{
    I2cData data;
    data.resize(2); // (LSB/MSB)
    if (i2c.readRegister(BMP280_I2C_ADDRESS, reg, data) != ESP_OK)
        return false;
    dig = (static_cast<int16_t>(data[1]) << 8) |
          static_cast<int16_t>(data[0]);
    return true;
}


bool Bmp280::getData(double& temperature, double& pressure)
{
    I2cData data;
    data.resize(BMP280_TEMPERATURE_DATA_SIZE + BMP280_PRESSURE_DATA_SIZE);

    // Read out all the value with in block is recomendet by vendor
    if (i2c.readRegister(BMP280_I2C_ADDRESS, BMP280_REG_PRESS_MSB, data) != ESP_OK)
        return false;

    auto rawPressure = static_cast<int32_t>(
          static_cast<int32_t>(data[BMP280_PRESSURE_MSB_DATA]) << 12
        | static_cast<int32_t>(data[BMP280_PRESSURE_LSB_DATA]) << 4
        | static_cast<int32_t>(data[BMP280_PRESSURE_XLSB_DATA]) >> 4
    );

    auto rawTemperature = static_cast<int32_t>(
          static_cast<int32_t>(data[BMP280_TEMPERATURE_MSB_DATA]) << 12
        | static_cast<int32_t>(data[BMP280_TEMPERATURE_LSB_DATA]) << 4
        | static_cast<int32_t>(data[BMP280_TEMPERATURE_XLSB_DATA]) >> 4
    );

    temperature = getCompensateTemperature(rawTemperature);
    pressure = getCompensatePressure(rawPressure);
    if (pressure == BMP280_INVALIDE_PRESSURE)
        return false;

    return true;
}

double Bmp280::getCompensateTemperature(int32_t rawTemperature)
{
    double x1 = 0.0;
	double x2 = 0.0;
	double temperature = 0.0;

	x1  = (static_cast<double>(rawTemperature) / 16384.0 - static_cast<double>(compData.dig_T1) / 1024.0) *
	      static_cast<double>(compData.dig_T2);

	x2  = ((static_cast<double>(rawTemperature) / 131072.0 - static_cast<double>(compData.dig_T1) / 8192.0) *
	       (static_cast<double>(rawTemperature) / 131072.0 - static_cast<double>(compData.dig_T1) / 8192.0)) *
	      static_cast<double>(compData.dig_T3);

    compData.t_fine = static_cast<int32_t>(x1 + x2);
	temperature  = (x1 + x2) / 5120.0;

    return temperature;
}

double Bmp280::getCompensatePressure(int32_t rawPressure)
{
    double x1 = 0.0;
	double x2 = 0.0;
	double pressure = 0.0;

	x1 = (static_cast<double>(compData.t_fine) / 2.0) - 64000.0;
	x2 = x1 * x1 * static_cast<double>(compData.dig_P6) / 32768.0;
	x2 = x2 + x1 * static_cast<double>(compData.dig_P5) * 2.0;
	x2 = (x2 / 4.0) + (static_cast<double>(compData.dig_P4) * 65536.0);
	x1 = (static_cast<double>(compData.dig_P3) * x1 * x1 / 524288.0 + static_cast<double>(compData.dig_P2) * x1) / 524288.0;
	x1 = (1.0 + x1 / 32768.0) * static_cast<double>(compData.dig_P1);
	pressure = 1048576.0 - static_cast<double>(rawPressure);
	/* Avoid exception caused by division by zero */
	if (x1 != 0.0)
		pressure = (pressure - (x2 / 4096.0)) * 6250.0 / x1;
	else
		return BMP280_INVALIDE_PRESSURE;
	x1 = static_cast<double>(compData.dig_P9) * pressure * pressure / 2147483648.0;
	x2 = pressure * static_cast<double>(compData.dig_P8) / 32768.0;
	pressure = pressure + (x1 + x2 + static_cast<double>(compData.dig_P7)) / 16.0;

	return pressure;
}