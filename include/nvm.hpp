#ifndef __ESBAROT_NVM_HPP__
#define __ESBAROT_NVM_HPP__

class Nvm
{
public:
    Nvm() = default;

    void init();
};

#endif // __ESBAROT_NVM_HPP__