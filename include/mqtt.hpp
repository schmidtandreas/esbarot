#ifndef __ESBAROT_MQTT_HPP__
#define __ESBAROT_MQTT_HPP__

#include <functional>
#include <string>
#include <stdint.h>
#include <esp_event_base.h>
#include <mqtt_client.h>

class Mqtt
{
public:
    using TempChanged = std::function<void(double)>;
    using StateChanged = std::function<void(bool)>;
public:
    Mqtt() = default;

    void init();
    void publishTemperature(double temperature);
    void publishTargetTemperature(double targetTemperature);
    void publishHeatState(bool heatState);
    void subscribeTargetTemperature(TempChanged targetTempChanged);
    void subscribeHeatState(StateChanged heatStateChanged);
public:
    static void eventHandlerWrapper(void* handlerArgs, esp_event_base_t base, int32_t eventId, void* eventData);

private:
    void eventHandler(void* handlerArgs, esp_event_base_t base, int32_t eventId, void* eventData);
    void publishState();

    static const std::string HA_CLIMATE_CFG_DATA;
    esp_mqtt_client_handle_t client{nullptr};
    bool heat{false};
    double targetTemperature{20};
    double temperature{0};
    TempChanged targetTempChanged{};
    StateChanged heatStateChanged{};
};

#endif // __ESBAROT_MQTT_HPP__