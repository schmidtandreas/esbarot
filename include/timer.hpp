#ifndef __ESBAROT_TIMER_HPP__
#define __ESBAROT_TIMER_HPP__

#include <functional>
#include <map>
#include <esp_timer.h>

class Timer
{
public:
    using TimerCallback = std::function<void(void)>;
    struct TimerCbData {
        TimerCallback callback;
    };
    struct TimerData {
        TimerCbData* cBdata;
        esp_timer_handle_t handle;
    };
    using TimerMap = std::map<std::string, TimerData>;
public:
    Timer() = default;

    void init();

    void registerTimer(const std::string& name, TimerCallback callback);
    void startTimer(const std::string& name, uint64_t period);
    void stopTimer(const std::string& name);
    void deleteTimer(const std::string& name);

private:
    static void timerHandlerWrapper(void* args);

private:
    TimerMap timers;
};

#endif // __ESBAROT_TIMER_HPP__