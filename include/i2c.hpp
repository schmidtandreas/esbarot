#ifndef __ESBAROT_I2C_HPP__
#define __ESBAROT_I2C_HPP__

#include <stdint.h>
#include <vector>
#include <esp_err.h>

using I2cData = std::vector<uint8_t>;
using I2cAddress = uint8_t;

class I2cMaster
{
public:
    I2cMaster() = default;

    void init();
    esp_err_t write(I2cAddress address, const I2cData& byteData, const I2cData& seqData);
    esp_err_t readRegister(I2cAddress address, I2cAddress regAddress, I2cData& data);
private:
    const uint32_t I2C_MASTER_FREQ_HZ{1000000};
    const int I2C_MASTER_SDA_IO{21};
    const int I2C_MASTER_SCL_IO{22};
};

#endif // __ESBAROT_I2C_HPP__