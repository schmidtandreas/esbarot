#ifndef __ESBAROT_WIFI_HPP__
#define __ESBAROT_WIFI_HPP__

#include <stdint.h>
#include <esp_event_base.h>
#include <functional>
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>

class Wifi
{
public:
    Wifi() = default;
    void init();
    bool connect();

public:
    static void eventHandlerWrapper(void* eventHandlerArg, esp_event_base_t eventBase, int32_t eventId, void* eventData);

private:
    void eventHandler(void* eventHandlerArg, esp_event_base_t eventBase, int32_t eventId, void* eventData);

private:
    esp_event_handler_instance_t eventInstanceAnyId;
    esp_event_handler_instance_t eventInstanceGotIp;
    EventGroupHandle_t eventGroupHandle;
    const uint8_t WIFI_CONNECTED_BIT{BIT0};
};

#endif // __ESBAROT_WIFI_HPP__