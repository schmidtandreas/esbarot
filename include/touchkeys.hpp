#ifndef __ESBAROT_TOUCHKEYS_HPP__
#define __ESBAROT_TOUCHKEYS_HPP__

#include <functional>
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include <driver/gpio.h>

class TouchKeys
{
private:
    struct IsrData{
        gpio_num_t gpio;
        TouchKeys* touckKeys;
    };
    using KeyEvent = std::function<void(bool)>;

public:
    TouchKeys();

    void init();

public:
    static void gpioTaskHandlerWrapper(void* args);
    static void gpioIsrHandlerWrapper(void* args);

    void registerLeftEvent(KeyEvent callback);
    void registerRightEvent(KeyEvent callback);

    bool getLeftKeyValue();
    bool getRightKeyValue();

private:
    void gpioTaskHandler(gpio_num_t gpio, bool pushed);

private:
   QueueHandle_t gpioEventQueue;
   IsrData isrDataLeft;
   IsrData isrDataRight;
   KeyEvent eventCallbackLeft{};
   KeyEvent eventCallbackRight{};

private:
    static const gpio_num_t GPIO_TOUCHKEY_LEFT{GPIO_NUM_16};
    static const gpio_num_t GPIO_TOUCHKEY_RIGHT{GPIO_NUM_17};
};

#endif // __ESBAROT_TOUCHKEYS_HPP__
