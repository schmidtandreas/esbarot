#ifndef __ESBAROT_BMP280_HPP__
#define __ESBAROT_BMP280_HPP__

#include "i2c.hpp"
#include <stdint.h>

class Bmp280
{
public:
    Bmp280(I2cMaster& i2c);

    void init();
    bool getData(double& temperature, double& pressure);

private:
    esp_err_t checkChipId();
    esp_err_t setRegister(I2cAddress reg, uint8_t value);
    esp_err_t getCompensationData();
    bool getCompensationValue(const I2cAddress reg, int16_t& dig);
    bool getCompensationValue(const I2cAddress reg, uint16_t& dig);
    double getCompensateTemperature(int32_t rawTemperature);
    double getCompensatePressure(int32_t rawPressure);

private:
    I2cMaster& i2c;

    struct {
        uint16_t dig_T1;
        int16_t dig_T2;
        int16_t dig_T3;
        uint16_t dig_P1;
        int16_t dig_P2;
        int16_t dig_P3;
        uint16_t dig_P4;
        int16_t dig_P5;
        int16_t dig_P6;
        uint16_t dig_P7;
        int16_t dig_P8;
        int16_t dig_P9;
        int32_t t_fine;
    } compData;

    const I2cAddress BMP280_I2C_ADDRESS{0x77};
    // Register addresses
    const I2cAddress BMP280_REG_ID{0xD0};
    const I2cAddress BMP280_REG_TEMP_MSB{0xFA};
    const I2cAddress BMP280_REG_PRESS_MSB{0xF7};
    const I2cAddress BMP280_REG_CTRL_MEAS{0xF4};
    const I2cAddress BMP280_REG_CONFIG{0xF5};
    const I2cAddress BMP280_REG_CALIB_T1{0x88};
    const I2cAddress BMP280_REG_CALIB_T2{0x8A};
    const I2cAddress BMP280_REG_CALIB_T3{0x8C};
    const I2cAddress BMP280_REG_CALIB_P1{0x8E};
    const I2cAddress BMP280_REG_CALIB_P2{0x90};
    const I2cAddress BMP280_REG_CALIB_P3{0x92};
    const I2cAddress BMP280_REG_CALIB_P4{0x94};
    const I2cAddress BMP280_REG_CALIB_P5{0x96};
    const I2cAddress BMP280_REG_CALIB_P6{0x98};
    const I2cAddress BMP280_REG_CALIB_P7{0x9A};
    const I2cAddress BMP280_REG_CALIB_P8{0x9C};
    const I2cAddress BMP280_REG_CALIB_P9{0x9E};
    // Miscullaneous constanst
    const uint8_t BMP280_ID{0x58};
    const uint8_t BMP280_TEMPERATURE_DATA_SIZE{3};
    const uint8_t BMP280_PRESSURE_DATA_SIZE{3};
    const uint8_t BMP280_TEMPERATURE_MSB_DATA{3};
    const uint8_t BMP280_TEMPERATURE_LSB_DATA{4};
    const uint8_t BMP280_TEMPERATURE_XLSB_DATA{5};
    const uint8_t BMP280_PRESSURE_MSB_DATA{0};
    const uint8_t BMP280_PRESSURE_LSB_DATA{1};
    const uint8_t BMP280_PRESSURE_XLSB_DATA{2};
    const uint8_t BMP280_CTRL_NORMAL{0x3};
    const uint8_t BMP280_CTRL_OSRS_P_16{0x5 << 2};
    const uint8_t BMP280_CTRL_OSRS_T_2{0x2 << 5};
    const uint8_t BMP280_CONF_IIR_16{0xF << 2};
    const uint8_t BMP280_CONF_T_SB_05{0x0 << 5};
    const double BMP280_INVALIDE_PRESSURE{-10000.0};
};

#endif // __ESBAROT_BMP280_HPP__